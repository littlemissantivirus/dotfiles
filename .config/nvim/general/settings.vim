let g:mapleader = "\<Space>"

syntax enable				" Enable syntax highlighting
set hidden				" Allows for multiple buffers
set nowrap				" No text wrapping!
set encoding=utf-8			" Set the displayed encoding.
set pumheight=10			" Set the popup menu height
set fileencoding=utf-8			" Set the file encoding
set ruler				" Always show the cursor position
set cmdheight=2				" More space for messages.
set iskeyword+=-			" Treat dash separated words as a word.
set mouse=a				" Enable the mouse
set splitbelow				" Horizontal splits will go below
set splitright				" Vertical splits will go to the right
set t_Co=256				" Enable 256 color support
set conceallevel=0			" Allows `` to be visible in markdown
set tabstop=2
set shiftwidth=2
set smarttab
set expandtab
set smartindent
set autoindent
set laststatus=0
set number
set cursorline
set background=dark
set showtabline=2
set noshowmode
set nobackup
set nowritebackup
set updatetime=300
set timeoutlen=100
set formatoptions-=cro			" Stop newlines automatically being comments.
set clipboard=unnamedplus 		" Global copy & paste.
au! BufWritePost $MYVIMRC source %
cmap w!! w !sudo tee %
